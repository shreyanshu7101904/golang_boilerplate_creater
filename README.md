[![Build Status](https://travis-ci.org/shreyanshu7101904/golang_boilerplate_creater.svg?branch=master)](https://travis-ci.org/shreyanshu7101904/golang_boilerplate_creater)

# golang Directory Builder
Create golang project structure
##installation libraries
`install requirements.txt
  pip3 install requirements.txt`
## usage

`
python3 golang.py --d /home/getmyuni/Music/github --p golan_proj --u shreyanshu7101904 --r pytho
`
## options
`
--d = directory name  : directory name where to create project
--p = folder name     : folder name for that project
--u = github username : github username for that project
--r = github repo name : github repository name
`
